(function() {
  var SwirlNode, Tree, TreeSwirl, height, swirls, width,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  width = 200;

  height = 400;

  swirls = [
    {
      color: 'yellow',
      nodes: 75,
      speed: 3,
      radius: 2
    }, {
      color: 'green',
      nodes: 200,
      speed: 3,
      radius: 1.5
    }, {
      color: 'orange',
      nodes: 75,
      speed: 3,
      radius: 2
    }, {
      color: 'green',
      nodes: 200,
      speed: 3,
      radius: 1.5
    }
  ];

  Tree = (function() {
    function Tree(w, h, swirls) {
      this.run = bind(this.run, this);
      var i;
      this.width = w;
      this.height = h;
      this.canvas = document.getElementById('tree');
      this.context = this.canvas.getContext('2d');
      this.canvas.width = w;
      this.canvas.height = h;
      this.swirls = (function() {
        var j, ref, results;
        results = [];
        for (i = j = 0, ref = swirls.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
          results.push(new TreeSwirl(this, swirls[i], i / swirls.length));
        }
        return results;
      }).call(this);
      this.run();
    }

    Tree.prototype.run = function(t) {
      if (t == null) {
        t = 0;
      }
      window.requestAnimationFrame(this.run);
      return this.draw(t);
    };

    Tree.prototype.draw = function(t) {
      var j, k, len, len1, ref, ref1, results, s;
      this.context.clearRect(0, 0, this.width, this.height);
      ref = this.swirls;
      for (j = 0, len = ref.length; j < len; j++) {
        s = ref[j];
        s.drawBack(t);
      }
      ref1 = this.swirls;
      results = [];
      for (k = 0, len1 = ref1.length; k < len1; k++) {
        s = ref1[k];
        results.push(s.drawFront(t));
      }
      return results;
    };

    return Tree;

  })();

  TreeSwirl = (function() {
    function TreeSwirl(tree, s, offset) {
      var i;
      this.tree = tree;
      this.offset = offset;
      this.color = s.color;
      this.speed = s.speed;
      this.radius = s.radius;
      this.nodes = (function() {
        var j, ref, results;
        results = [];
        for (i = j = 0, ref = s.nodes; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
          results.push(new SwirlNode(this, i / s.nodes));
        }
        return results;
      }).call(this);
    }

    TreeSwirl.prototype.drawBack = function(t) {
      var j, len, n, ref, results;
      ref = this.nodes;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        n = ref[j];
        if (n.inBack(t)) {
          results.push(n.draw(t));
        }
      }
      return results;
    };

    TreeSwirl.prototype.drawFront = function(t) {
      var j, len, n, ref, results;
      ref = this.nodes;
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        n = ref[j];
        if (n.inFront(t)) {
          results.push(n.draw(t));
        }
      }
      return results;
    };

    return TreeSwirl;

  })();

  SwirlNode = (function() {
    function SwirlNode(swirl, offset) {
      this.swirl = swirl;
      this.offset = offset;
    }

    SwirlNode.prototype.yPos = function() {
      var d, od;
      d = this.t / 1000 * this.swirl.speed;
      od = d + this.offset * this.swirl.tree.height;
      return (this.swirl.tree.height - od % this.swirl.tree.height) % this.swirl.tree.height;
    };

    SwirlNode.prototype.xDeg = function() {
      return this.yPos() * 5 + 360 * this.swirl.offset;
    };

    SwirlNode.prototype.xRad = function() {
      return this.xDeg() * Math.PI / 180;
    };

    SwirlNode.prototype.xPos = function() {
      return Math.sin(this.xRad()) * this.swirl.tree.width * this.yPos() / this.swirl.tree.height / 2 + this.swirl.tree.width / 2;
    };

    SwirlNode.prototype.shade = function() {
      return (Math.cos(this.xRad()) + 1) / 3;
    };

    SwirlNode.prototype.inBack = function(t) {
      this.t = t;
      return Math.cos(this.xRad()) > 0;
    };

    SwirlNode.prototype.inFront = function(t) {
      this.t = t;
      return !this.inBack(t);
    };

    SwirlNode.prototype.draw = function(t) {
      this.t = t - 180;
      this.drawNode(this.swirl.radius * 0.6, this.shade() + 0.4);
      this.t = t - 90;
      this.drawNode(this.swirl.radius * 0.8, this.shade() + 0.2);
      this.t = t;
      return this.drawNode(this.swirl.radius, this.shade());
    };

    SwirlNode.prototype.drawNode = function(size, shade) {
      var c;
      c = this.swirl.tree.context;
      c.beginPath();
      c.arc(this.xPos(), this.yPos(), size, 0, 2 * Math.PI);
      c.fillStyle = this.swirl.color;
      c.fill();
      c.fillStyle = "rgba(0,0,0," + shade + ")";
      return c.fill();
    };

    return SwirlNode;

  })();

  new Tree(width, height, swirls);

}).call(this);